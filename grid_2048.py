import random as r
import copy

def create_grid(size):
    game_grid = []
    for i in range(0,size):
        game_grid.append([0 for k in range (size)])
    return game_grid

def grid_add_new_tile_at_position(game_grid,i,j):
    L=[2 for k in range(9)]
    L.append(4)
    game_grid[i][j]=r.choice(L)
    return game_grid

def get_all_tiles(game_grid):
    L=[]
    size=len(game_grid)
    for i in range(size):
        for j in range(size):
            if type(game_grid[i][j])==int:
                L.append(game_grid[i][j])
            else:
                L.append(0)
    return L

def get_empty_tiles_positions(game_grid):
    position=[]
    size=len(game_grid)
    for i in range(size):
        for j in range(size):
            if type(game_grid[i][j])!=int or game_grid[i][j]==0:
                position.append((i,j))
    return position

def grid_add_new_tile(game_grid):
    L=[2 for k in range(9)]
    L.append(4)
    position=get_empty_tiles_positions(game_grid)
    i,j=r.choice(position)
    game_grid[i][j]=r.choice(L)
    return game_grid

def get_new_position(game_grid):
    L=get_empty_tiles_positions(game_grid)
    return r.choice(L)

def grid_get_value(game_grid,x,y):
    if type(game_grid[x][y])!=int or game_grid[x][y]==0:
        return 0
    else:
        return game_grid[x][y]

def init_game(size):
    game_grid=create_grid(size)
    game_grid=grid_add_new_tile(game_grid)
    game_grid=grid_add_new_tile(game_grid)
    return game_grid

def grid_to_string(game_grid):
    grid=copy.deepcopy(game_grid)
    size=len(grid)
    string=[]
    for i in range(size):
        for j in range(size):
            if grid[i][j]==0:
                grid[i][j]=' '
    for i in range(size):
        string.append(list_to_str([' ===' for k in range(size)] + [' \n']))
        string.append(list_to_str(['| ' + str(grid[i][j]) + ' ' for j in range(size)]))
        string.append('|\n')
    string.append(list_to_str([' ===' for k in range(size)] + [' ']))
    return list_to_str(string)

def list_to_str(l):
    chn = "".join(l)
    return chn

def long_value(grid):
    size=len(grid)
    n=0
    for i in range(size):
        for j in range(size):
            m=len(str(grid[i][j]))
            if n<m:
                n=m
    return n

def grid_to_string_size(game_grid):
    size=len(game_grid)
    long=long_value(game_grid)
    string=[]
    for i in range(size):
        for j in range(size):
            if game_grid[i][j]==0:
                game_grid[i][j]=' '
    for i in range(size):
        string.append(list_to_str([' ' + '=' * (long + 2) for k in range(size)] + [' \n']))
        string.append(list_to_str(['| ' + case_adaptee(game_grid[i][j], long) + ' ' for j in range(size)]))
        string.append('|\n')
    string.append(list_to_str([' ' + '=' * (long + 2) for k in range(size)] + [' ']))
    return list_to_str(string)

def case_adaptee(j,long):
    t=len(str(j))
    if (long-t)%2==0:
        k=(long-t)//2
        return ' '*k+str(j)+' '*k
    else:
        k=(long-t)//2
        return ' '*(k+1)+str(j)+' '*k


THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32:
"32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048",
4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8:
"Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048:
"Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8:
"C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K",
4096: "L", 8192: "M"}}


def long_value_with_theme(grid,theme):
    size=len(grid)
    n=0
    for i in range(size):
        for j in range(size):
            if type(grid[i][j])!=int:
                grid[i][j]=0
            m=len(str(theme[grid[i][j]]))
            if n<m:
                n=m
    return n

def grid_to_string_with_size_and_theme(game_grid,theme):
    size=len(game_grid)
    grid=copy.deepcopy(game_grid)
    grid_theme=[[] for k in range(size)]
    long=long_value_with_theme(grid,theme)
    string=[]
    for i in range(size):
        for j in range(size):
            if type(grid[i][j])!=int or grid[i][j]==0:
                grid_theme[i].append(' ')
            else:
                grid_theme[i].append(theme[grid[i][j]])
    for i in range(size):
        string.append(list_to_str([' ' + '=' * (long + 2) for k in range(size)] + [' \n']))
        string.append(list_to_str(['| ' + case_adaptee(grid_theme[i][j], long) + ' ' for j in range(size)]))
        string.append('|\n')
    string.append(list_to_str([' ' + '=' * (long + 2) for k in range(size)] + [' ']))
    return list_to_str(string)

def move_row_left(l):
    s=0
    n=len(l)
    for i in range(n):
        if type(l[i])==int and l[i]>0:
            if i!=s:
                l[s]=l[i]
                l[i]=0
            s+=1
    for i in range(n-1):
        if l[i]==l[i+1]:
            l[i]+=l[i]
            l[i+1]=0
    s=0
    for i in range(n):
        if type(l[i])==int and l[i]>0:
            if i!=s:
                l[s]=l[i]
                l[i]=0
            s+=1
    return l

def move_row_right(l):
    l.reverse()
    l=move_row_left(l)
    l.reverse()
    return l

def move_grid(grid,di):
    size=len(grid)
    if di in'gd':
        for i in range(size):
            if di=='d':
                grid[i]=move_row_right(grid[i])
            else:
                grid[i]=move_row_left(grid[i])
    else:
        for j in range(size):
            l=[]
            for i in range(size):
                l.append(grid[i][j])
            for i in range(size):
                if di=='h':
                    l=move_row_left(l)
                else:
                    l=move_row_right(l)
            for i in range(size):
                grid[i][j]=l[i]
    return grid

def is_grid_full(grid):
    size=len(grid)
    full=True
    for i in range(size):
        for j in range(size):
            if type(grid[i][j])!=int or grid[i][j]==0:
                full=False
    return full

def move_possible(grid):
    L=[]
    for di in ['g','d','h','b']:
        l=copy.deepcopy(grid)
        l=move_grid(l,di)
        L.append(grid!=l)
    return L

def move_possible_direction(grid):
    L=[]
    for di in ['g','d','h','b']:
        l=copy.deepcopy(grid)
        l=move_grid(l,di)
        #print(grid,l,di,grid==l)
        if grid!=l:
            L.append(di)
    return L

def is_game_over(grid):
    return not True in move_possible(grid)

def get_grid_tile_max(grid):
    return max(get_all_tiles(grid))


def random_play():
    grid=init_game(4)
    print(grid)
    print(grid_to_string_size(grid))
    over=False
    while not over:
        move_pos=move_possible_direction(grid)
        di=r.choice(move_pos)
        grid=move_grid(grid,di)
        grid_add_new_tile(grid)
        print(grid_to_string_size(grid))
        over=is_game_over(grid)
    if get_grid_tile_max(grid)>=2048:
        print('Vous avez gagné')
    else:
        print('Vous avez perdu')


