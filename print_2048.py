import tkinter as tk
from tkinter import DoubleVar, Label, Spinbox
from game2048.grid_2048 import *
from game2048.textual_2048 import *
from functools import partial
import pickle

game_grid=0
graphical_grid=0
background=0
grid_size=0


TILES_BG_COLOR = {0: "#9e948a", 2: "#eee4da", 4: "#ede0c8", 8: "#f1b078", \
                  16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b", \
                  128: "#edcf72", 256: "#edcc61", 512: "#edc850", \
                  1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92", \
                  8192: "#24ba63"}

TILES_FG_COLOR = {0: "#776e65", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2", \
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}

TILES_FONT = {"Verdana", 40, "bold"}

Score_font= {"Comic sans ms", 40, "bold"}

"""
def graphical_grid_init():
    window = tk.Tk()
    window.title('2048')
    lvl=tk.Toplevel(window)
    lvl.title('2048')
    lvl.grid()
    grid_size=4
    grid_game=init_game(grid_size)
    background=tk.Frame(window,bd=1)
    background.grid()
    graphical_grid=[[] for k in range(grid_size)]
    for i in range(grid_size):
        for j in range(grid_size):
            graphical_grid[i].append(tk.Frame(background,bd=0.5,bg="#9e948a",height=50,width=50,relief='solid'))
            graphical_grid[i][j].grid(row=i,column=j)
    display_and_update_graphical_grid(grid_game,graphical_grid,background,grid_size)
    window.mainloop()
"""

def update_label(spinbox, label, var):
    """
    Écrit 'min' ou 'max' dans label en fonction de la valeur
    du textvariable de spinbox
    """
    value = var.get()
    """
    if value == spinbox.cget('from'):
        label.config(text='Min')
    elif value == spinbox.cget('to'):
        label.config(text='Max')
    """

def show_selection(label, choices, listbox):
    choices = choices.get()
    text = ""
    for index in listbox.curselection():
        text += choices[index] + " "

    label.config(text=text)




def game_play():
    global game_grid
    def display_and_update_graphical_grid(game_grid, graphical_grid, background, grid_size):
        for i in range(grid_size):
            for j in range(grid_size):
                valeur = game_grid[i][j]
                if valeur == 0:
                    graphical_grid[i][j].config(bg=TILES_BG_COLOR[game_grid[i][j]], fg=TILES_FG_COLOR[game_grid[i][j]], text="")
                else:
                    graphical_grid[i][j].config(bg=TILES_BG_COLOR[game_grid[i][j]], fg=TILES_FG_COLOR[game_grid[i][j]], text=theme[game_grid[i][j]])
    def sauvegarder_partie():
        global game_grid
        jeu=game_grid
        s=pickle.dumps(jeu)
        f=open("sauvegarde","wb")
        f.write(s)
        f.close()
    def charger_partie():
        global game_grid
        global graphical_grid
        global background
        global grid_size
        f=open("sauvegarde","rb")
        data=pickle.loads(f.read())
        game_grid=data
        display_and_update_graphical_grid(game_grid, graphical_grid, background, grid_size)

    #Menu de base
    menu = tk.Tk()
    menu.title('Menu 2048')

    #Size
    value = DoubleVar(menu)
    label = Label(text='Taille:')
    spinbox = Spinbox(menu, textvariable=value, from_=2, to=10, increment=1)
    spinbox.config(command=partial(update_label, spinbox, label, value))
    spinbox.grid(row=1, column=0)
    label.grid(row=0, column=0)

    #Theme
    label = Label(text='Choisir un theme')
    label.grid(row=2,column=0)
    choices = tk.Variable(menu, ('Default', 'Chemistry', 'Alphabet'))
    listbox = tk.Listbox(menu, listvariable=choices, selectmode="browse",exportselection=False)
    label = Label(menu, text='')

    #Sauvegarder & charger
    sauvegarde_button = tk.Button(menu, text='Sauvegarder la partie', command=sauvegarder_partie)
    charger_button = tk.Button(menu, text='Charger la partie', command=charger_partie)
    sauvegarde_button.grid(row=40)
    charger_button.grid(row=41)

    #Couleur
    couleurs = tk.Variable(menu, ('Couleur 1', 'Couleur 2', 'Couleur 3','Couleur 4'))
    Label(text='Choisir une couleur').grid(row=7,column=0)
    list_color=['#F0E68C','#FF00FF','#FFAAAA','#F0E68C']
    frame=tk.Label(menu,text='                      ')
    frame.grid(row=6,column=0)
    for i in range(len(list_color)):
        Label(frame,bg=list_color[i],width = 2).place(relx=i/len(list_color),rely=0)
    listbox_couleur = tk.Listbox(menu, listvariable=couleurs, selectmode="browse",exportselection=False)

    listbox_couleur.grid(row=9,column=0)



    def enter_selection(listbox=listbox,value=value):
        global theme
        global game_grid
        theme_num=listbox.curselection()[0]
        color_num=listbox_couleur.curselection()[0]
        theme=THEMES[str(theme_num)]
        color=list_color[color_num]
        grid_size=int(value.get())
        jeu = tk.Toplevel(menu)
        jeu.title('2048')
        jeu.grid()
        score=0
        score_label=Label(jeu,bg=color,fg='white',text='Score: '+str(score),font=("Comic sans ms", 40))
        score_label.grid(row=0,column=0)
        game_grid = init_game(grid_size)
        background = tk.Frame(jeu,bd=1)
        background.grid()
        labels = []
        graphical_grid = [[] for k in range(grid_size)]
        for i in range(grid_size):
            labels.append([])
            for j in range(grid_size):
                graphical_grid[i].append(tk.Label(master = background, bd = 1,relief = "ridge", bg = TILES_BG_COLOR[0], text = "",height = 5, width = 10))
                graphical_grid[i][j].grid(column = j, row = i+1)
        display_and_update_graphical_grid(game_grid,graphical_grid,background,grid_size)
        over=False
        def keypress(event):
            global game_grid
            touche = event.keysym
            move_possible=move_possible_direction(game_grid)
            direction={'Up':'h','Down':'b','Left':'g','Right':'d'}
            if direction[touche] in move_possible_direction(game_grid):
                if touche == 'Up':
                    game_grid = move_grid(game_grid, 'h')
                elif touche == 'Down':
                    game_grid = move_grid(game_grid, 'b')
                elif touche == 'Right':
                    game_grid = move_grid(game_grid, 'd')
                elif touche == 'Left':
                    game_grid = move_grid(game_grid, 'g')
                game_grid = grid_add_new_tile(game_grid)
                tot=get_all_tiles(game_grid)
                score=sum(tot)
                score_label.config(text='Score: '+str(score))
            display_and_update_graphical_grid(game_grid,graphical_grid,background,grid_size)
            over=is_game_over(game_grid)
            if over:
                if get_grid_tile_max(game_grid) < 2048:
                    Frame=tk.Label(jeu,text='Vous avez perdu \n Score:'+str(score),bg='black',fg='white',font=('Comic sans ms',20))
                    Frame.place(relx=0.5,rely=0.5,anchor='center')

                else:
                    Frame=tk.Label(jeu,text='Vous avez gagné\n Score:'+str(score),bg='yellow',fg='black',font=('Comic sans ms',20))
                    Frame.place(relx=0.5,rely=0.5,anchor='center')

        display_and_update_graphical_grid(game_grid,graphical_grid,background,grid_size)
        jeu.bind("<Key>", keypress)
    button = tk.Button(menu, text='Jouer', command=enter_selection)
    listbox.grid(row=3, column=0)
    button.grid(row=10, column=0)
    label.grid(row=11, column=0)


    menu.mainloop()


game_play()



